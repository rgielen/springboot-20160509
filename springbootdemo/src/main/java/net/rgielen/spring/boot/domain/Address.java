package net.rgielen.spring.boot.domain;

import javax.persistence.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "Address.findByUsername",
                query = "select a from Address a where a.user.userName=:username"
        )
} )
@SequenceGenerator(name = "adr_seq", sequenceName = "adr_seq")
public class Address {

    @Id
    @GeneratedValue(generator = "adr_seq")
    private Long id;
    private String street;
    private String zip;
    private String city;
    @ManyToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
