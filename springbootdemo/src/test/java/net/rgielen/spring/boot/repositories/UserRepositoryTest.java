package net.rgielen.spring.boot.repositories;

import net.rgielen.spring.boot.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    public void testUserGetsFound() throws Exception {
        User user = new User("Rene", "Gielen", "rgielen");
        User save = userRepository.save(user);

        List<User> users = userRepository.findByLastnameOrFirstnameAlt("Rene", "gfgf");
        assertNotNull(users);
        assertTrue(users.size()>0);
    }

}