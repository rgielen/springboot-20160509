package net.rgielen.spring.basics.repositories;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public interface TracingRepository {

    void logStartTime();
}
