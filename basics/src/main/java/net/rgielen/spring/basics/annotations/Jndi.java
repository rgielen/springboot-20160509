package net.rgielen.spring.basics.annotations;

import org.springframework.context.annotation.Profile;

import java.lang.annotation.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Profile("jndi")
public @interface Jndi {
}
