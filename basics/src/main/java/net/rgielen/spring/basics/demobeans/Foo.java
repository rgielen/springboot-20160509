package net.rgielen.spring.basics.demobeans;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class Foo {

    Bar bar;

    public Foo() {
        System.out.println("Constructor called");
        System.out.println("Curremt state of bar " + bar);
    }

    public void setBar(Bar bar) {
        System.out.println("Setter called with " + bar);
        this.bar = bar;
    }

    @Override
    public String toString() {
        return "Foo{" +
                "bar=" + bar +
                '}';
    }
}
