package net.rgielen.spring.basics.demobeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Component
public class DummyService implements GenericService {

    final DummyDao dao;
    final Foo foo;

    @Autowired
    public DummyService(DummyDao dao, Foo foo) {
        this.dao = dao;
        this.foo = foo;
        System.out.println("Initaialized. Dao is " + dao);
    }

    public int doSomethingWithDao(Long id) {
        System.out.println(foo.toString());
        return dao.get(id).hashCode();
    }

    @PostConstruct
    void init() {
        System.out.println("Nothing to see here any longer");
    }

    @PreDestroy
    void destroy() {
        System.out.println("Bye");
    }
}
