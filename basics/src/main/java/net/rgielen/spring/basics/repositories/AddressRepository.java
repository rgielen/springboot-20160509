package net.rgielen.spring.basics.repositories;

import net.rgielen.spring.basics.entities.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public interface AddressRepository extends JpaRepository<Address, Long>{

    List<Address> findAllByUserLastname(String lastname);
}
