package net.rgielen.spring.basics;

import net.rgielen.spring.basics.annotations.TestEnvironment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringBasicsConfig.class, SpringBasicsTestConfig.class})
public class SpringBasicsConfigTest {

    @Autowired @TestEnvironment
    DataSource dataSource;

    @Autowired
    Environment env;

    @Value("${foo}")
    String foo;

    @Test
    public void testDataSourceIsPresentAndManaged() throws Exception {
        assertNotNull(dataSource);
        System.out.println("DATASOURCE: " + dataSource);
    }

    @Test
    public void testPropertySourceWorks() throws Exception {
        assertEquals("bar", env.getProperty("foo"));

        System.out.println(foo);
    }
}