package net.rgielen.spring.basics.repositories;

import net.rgielen.spring.basics.SpringBasicsConfig;
import net.rgielen.spring.basics.entities.User;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertEquals;


/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringBasicsConfig.class)
@Transactional
public class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    public void testUserRepositoryExists() throws Exception {
        assertNotNull(userRepository);
    }

    @Test
    public void testInsertUser() throws Exception {
        final User user = new User("foo", "bar", "foobar");
        userRepository.save(user);
        assertNotNull(user.getId());
        User loadedUser = userRepository.findOne(user.getId());
        assertNotNull(loadedUser);
        assertEquals(user.getId(), loadedUser.getId());
    }

    @Test
    public void testCustomMethodGetsMixedIn() throws Exception {
        final User user = new User("foo", "bar", "foobar");
        assertThat(user.getFirstname())
                .isEqualToIgnoringCase(
                        userRepository.createJsonRepresentation(user)
                );
    }

    @Test
    public void testFindByUsernameWorks() throws Exception {
        final User user = new User("foo", "bar", "foobar");
        userRepository.save(user);
        final User foobar = userRepository.findFirstByUserName("foobar");
        assertThat(foobar).isNotNull();
    }

}