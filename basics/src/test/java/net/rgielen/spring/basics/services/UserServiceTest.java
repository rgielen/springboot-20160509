package net.rgielen.spring.basics.services;

import net.rgielen.spring.basics.SpringBasicsConfig;
import net.rgielen.spring.basics.SpringBasicsTestConfig;
import net.rgielen.spring.basics.entities.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringBasicsConfig.class, SpringBasicsTestConfig.class})
@Transactional
public class UserServiceTest {

    @Autowired
    UserService userService;

    @Test
    @Rollback(value = false)
    public void testUserCanBeSaved() throws Exception {
        userService.save(new User());
    }

}